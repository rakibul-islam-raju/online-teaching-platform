from datetime import date
from rest_framework import serializers
from .models import Teacher


# Teacher detail serializer
class TeacherDetailSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()

    class Meta:
        model = Teacher
        fields = '__all__'
        include = ['age']

    def get_full_name(self, obj):
        if obj.middle_name:
            fullname = obj.first_name + ' ' + obj.middle_name + ' ' + obj.last_name
        else:
            fullname = obj.first_name + ' ' + obj.last_name
        return fullname

    def get_age(self, obj):
        birth_date = obj.date_of_birth
        today = date.today()
        age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
        return age


# Teacher create serializer
class CreateTeacherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Teacher
        exclude = ['status', 'created_at', 'updated_at']
