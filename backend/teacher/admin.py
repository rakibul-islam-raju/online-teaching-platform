from django.contrib import admin
from .models import Teacher


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'email', 'phone_number', 'blood_group', 'gender', 'marital_status',
                    'status']
    list_filter = ['gender', 'marital_status', 'status']


admin.site.register(Teacher, TeacherAdmin)
