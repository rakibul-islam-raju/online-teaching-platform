from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from .models import Teacher
from .serializers import (CreateTeacherSerializer,
                          TeacherDetailSerializer)


class TeacherListCreateView(ListCreateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Teacher.objects.filter(status=True)
    serializer_class = CreateTeacherSerializer


class TeacherDetailUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'pk'
    queryset = Teacher.objects.filter(status=True)
    serializer_class = TeacherDetailSerializer
