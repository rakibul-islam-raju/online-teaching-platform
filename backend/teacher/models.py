from django.db import models


class Teacher(models.Model):
    GENDER_CHOICES = (
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    )

    MARITAL_STATUS_CHOICES = (
        ('single', 'Single'),
        ('married', 'Married'),
        ('divorced', 'Divorced'),
        ('widowed', 'Widowed')
    )

    BLOOD_GROUP_CHOICES = (
        ('a+', 'A+'),
        ('a-', 'A-'),
        ('b+', 'B+'),
        ('b-', 'B-'),
        ('o+', 'O+'),
        ('o-', 'O-'),
        ('ab-', 'AB-'),
    )

    first_name = models.CharField(max_length=35)
    middle_name = models.CharField(max_length=35, blank=True, null=True, help_text='Optional')
    last_name = models.CharField(max_length=35)
    father_name = models.CharField(max_length=75)
    date_of_birth = models.DateField()
    email = models.EmailField(max_length=255)
    phone_number = models.CharField(max_length=15)
    emergency_phone_number = models.CharField(max_length=15)
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES)
    blood_group = models.CharField(max_length=5, choices=BLOOD_GROUP_CHOICES)
    marital_status = models.CharField(max_length=8, choices=MARITAL_STATUS_CHOICES)
    address = models.TextField(max_length=255)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.email
