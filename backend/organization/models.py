from django.db import models

# Create your models here.
class Organization(models.Model):
    TYPE = (
        ('educational','Educational'),
        ('marketing','Marketing'),
        ('software','Software'),
        ('government','Government'),
        ('others','Others'),
    )

    
    name = models.CharField(max_length=150)
    phone = models.CharField(max_length=50)
    starting_date = models.DateField(blank=True, null=True)
    type_of_organization = models.CharField(max_length=50,choices = TYPE)
    numbers_of_staffs = models.IntegerField()

    def __str__(self):
        return self.name
    
    
 