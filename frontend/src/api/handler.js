import axios from 'axios'


const url = process.env.url

export function fetchData() {
    axios.get(url)
        .then(res=>
        {
            const result= res.data
            return result
        })
        .catch(err=> err)
}

export function postData(data) {
    axios.post(url, data).then(result=> {
        return result
    }).catch(err=>err)
}

export function editData(data) {
    axios.put(url,data).then(result=>{
        return result
    }).catch(err=> err)
}

export function deleteData(id) {
    axios.delete(`${url}/${id}`).then(result=> {
        return result
    }).catch(err=> err)
}
